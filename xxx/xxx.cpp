// xxx.cpp: определяет точку входа для консольного приложения.
//

#include "stdafx.h"
#include <iostream>
#include <fstream>
#include <string>
#include <openssl/conf.h> // функции, структуры и константы настройки OpenSSl 
#include <openssl/evp.h> // сами криптографические функции 
#include <openssl/err.h> // коды внутренних ошибок OpenSSL и их расшифровка 



using namespace std;


	

int main()
{
	int select;
	bool Flag = true;
	do
	{	
		cout << "1 - Write\n2 - Read\n3 - Exit" << endl;
		cin >> select;
		switch (select)
		{
		case 1:
		{
			string Familia;
			string Name;
			string Pasport;
			ofstream dannye("dannye.txt", ios::app);

			cout << "Enter Second Name " << endl; //Ввод фамилии
			cin >> Familia;

			cout << "Enter Name" << endl; //Ввод имени
			cin >> Name;

			cout << "Pasport" << endl; //Ввод паспорта
			cin >> Pasport;
			bool Name_Flag = true;
			bool Familia_flag = true;

			for (int i = 0; i < Name.length(); i++) // идем по символам
			{ 
				if (Name[i] <= 'A' && Name[i] >= 'Z' || Name[i] <= 'a' && Name[i] >= 'z')
				{
					Name_Flag = false;
				}
			}

			for (int i = 0; i < Familia.length(); i++) // идем по символам
			{
				if (Familia[i] <= 'A' && Familia[i] >= 'Z' || Familia[i] <= 'a' && Familia[i] >= 'z')
				{
					Familia_flag = false;
				}
			}

		
			if (Pasport.length() == 10 && (atoi(Pasport.c_str()) != 0) && Name_Flag == true && Familia_flag == true)
			{


				dannye << "Familia: " << Familia << "  " << "Name: " << Name << "  " << "Pasport: " << Pasport << endl; //выводит все данные вместе
				dannye.close();

				unsigned char *key = (unsigned char*)"0123456789"; //ключ шифрования
				unsigned char *iv = (unsigned char*)"0123456789012345"; //инициализирующий вектор


				EVP_CIPHER_CTX *ctx; //Создаётся указатель на несуществующую структуру

				ctx = EVP_CIPHER_CTX_new(); // создание структуры с настройками метода 

				int length;//длина полученного шифра

				EVP_EncryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv);// инициализация методом AES, ключом и вектором 



				fstream f0, f_encrypted;
				f0.open("dannye.txt", std::fstream::in | std::fstream::binary); //файл для исходных данных

				f_encrypted.open("encrypted_dannye.txt", std::fstream::out | std::fstream::trunc | std::fstream::binary);//файл для зашифрованных данных

				char buffer[256] = { 0 };//буфер, где будут храниться данные которые будут шифроваться
				char out_buf[256] = { 0 };//буфер, где будут храниться уже зашифрованные данные

				ctx = EVP_CIPHER_CTX_new();//Создание структуры с настройками метода
				EVP_EncryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv);// инициализация методом AES, ключом и вектором 
				length = 0;//длина полученного шифра
				f0.read(buffer, 256);//считыание исходынх данных в буфер

				while (f0.gcount() > 0) //цикл, пока из файла что-то считывается (пока размер считанной порции>0)
				{
					//Шифрование порции
					EVP_EncryptUpdate(ctx, // объект с настройками
						(unsigned char *)out_buf, //выходной параметр: ссылка, куда помещать зашифрованные данные
						&length, //выходной параметр: длина полученного шифра
						(unsigned char *)buffer, // входной параметр: что шифровать
						f0.gcount()); //входной параметр: длина входных данных


					f_encrypted.write(out_buf, length);//запись зашифрованной порции в файл


					f0.read(buffer, 256);//считывание следующей порции

				}

				EVP_EncryptFinal_ex(ctx, (unsigned char*)out_buf, &length);//завершение шифрования
				f_encrypted.write(out_buf, length);//запись последней зашифрованной порции в файл
				f_encrypted.close();//закрытие файла с зашифрованными данными
				f0.close();//закрытие файла с исходными данными


				fstream f_decrypted;
				f_encrypted.open("encrypted_dannye.txt", std::fstream::in | std::fstream::binary);//файл для зашифрованных файлов

				f_decrypted.open("decrypted_dannye.txt", std::fstream::out | std::fstream::trunc | std::fstream::binary);//файл для расшифрованных файлов


				ctx = EVP_CIPHER_CTX_new();// создание структуры с настройками метода 

				EVP_DecryptInit_ex(ctx, EVP_aes_256_cbc(), NULL, key, iv); // инициализация методом AES, ключом и вектором 
				length = 0;//длина полученного шифра
				f_encrypted.read(buffer, 256);
				while (f_encrypted.gcount() > 0)
				{
					EVP_DecryptUpdate(ctx,//объект с натройками
						(unsigned char *)out_buf, //ссылка, куда помещать расшифрованные данные
						&length, //длина полученного шифра
						(unsigned char *)buffer, //что расшифровывать
						f_encrypted.gcount());//длина входных данных

					//вывод зашифрованной порции
					f_decrypted.write(out_buf, length);

					//считывание следующей порции
					f_encrypted.read(buffer, 256);

				}
				EVP_DecryptFinal_ex(ctx, (unsigned char*)out_buf, &length);//завершение дешифровки
				f_decrypted.write(out_buf, length);//запись последней порции в файл
				f_decrypted.close();//закрытие файла с расшифрованными данными
				f_encrypted.close();//закрытие файла с зашифрованными данными
			}
			else
			{
				cout << "ERROR, you made mistake!" << endl;
			}
			break;
		}
		case 2:
		{

			const int N = 256; //Константный размер строки 
			char buff[N] = ""; //В S будут считываться строки 
			fstream f_decrypted;
			f_decrypted.open("decrypted_dannye.txt", std::fstream::in);//файл для расшифрованных файлов
			
			while (!f_decrypted.eof()) //Будем читать информацию пока не дойдем до конца файла
			{
				f_decrypted.getline(buff, N); //Построчное считывание информации в S 
				cout << buff << endl; //Вывод очередной строки на экран 
			}
			f_decrypted.close();  //Закрыли открытый файл
			break;
		}
		case 3:
		{
			Flag = false;
		}
		default:
		{
			break;
		}
		
		}
	} while (Flag);


	return 0;

}


